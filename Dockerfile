FROM python:3.6-slim
MAINTAINER Sérgio Berlotto <sergio.berlotto@gmail.com>

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /code

RUN pip install --upgrade pip
RUN pip install pipenv
COPY ./Pipfile /code/Pipfile
RUN pipenv install --deploy --system --skip-lock --dev

ADD ./jetbovapi/ /code/
