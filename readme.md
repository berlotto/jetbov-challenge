
#Jetbov challenge


Django e DRF foram definidos no teste, apenas foi adicionado a lib django-filter para tratar os filtros do endpoint por automatizar este processo.

Docker é utilizado para muitos ambientes, então, para rodar o aplicativo tanto em dev como em prod não tem melhor.

Aqui apenas deixei o banco de dados como sqlite padrão do Django, mas facilmente podemos subir um banco (postgresql por exemplo) também, em paralelo ao app para conectar ambos e deixarmos a app mais robusta.

Não foi feita uma interface web por temos o Admin como interface já pronta e o foco é produzir uma boa API.

Testes

Nos testes, foquei em apresentar alguns tipos de testes que podemos fazer na aplicação API. Não estão todos os endpoints testados por ficar muito extenso e tomar muito tempo esta parte, então tentei apresentar como faria com apenas estes testes. A ampliação é necessária para que todas as situações necessárias (boas, ruins, esperadas, não esperadas, limites, etc) sejam tratadas pelos testes.

Pipenv - Utilizar o pipenv é prático pois ele praticamente une duas funcionalidades em 1 só (virtualenv e pip).

Utilizei o bitbucket pipelines para fazer a verificação dos testes e lint (flake8) que deve ser feita sempre quando commits são feitos.

A questão dos commits, foram feitos poucos commits devido a inicialização do projeto, criar uma estrutura inicial, testando o ambiente e tendo que commitar muito granularmente acarreta em uma demora maior para a construção. Então, fiz todo o projeto que precisa inicialmente e a partir de agora passei a commitar conforme alterações fossem sendo necessárias. Geralmente faço meuis commits quando tenho algo funcional em mãos, não costumo commitar partes quebradas de código, assim como as pull-requests, elas são geralmente pequenas, que facilitam a correção e validação por parte do time e nunca vão com códigos quebrados.

Para executar a aplicação, através de um arquivo Makefile e do docker não temos trabalho.

Estes são os passos:

    git clone https://bitbucket.org/berlotto/jetbov-challenge.git
    cd jetbovapi
    make install

Depois disto a aplicação estará rodando no endereço: http://localhost:8000 .

Para o primeiro acesso será necessário criar um usuário:

    make run
    docker-compose exec jetbov python manage.py createsuperuser

Depois basta acessar o /admin e criar os dados das fazendas, animais e pesagens.

A Api está disponível em `http://localhost:8000/api`

A documentação da API está disponível no diretório: docs, arquivo `api.html`. É necessário a ferramenta `aglio` instalada para atualizar a documentação.

    sudo npm -g install aglio

A documentação é criada na linguagem de documentação API Blueprint [https://apiblueprint.org]
