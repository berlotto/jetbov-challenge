from rest_framework import serializers

from farms.models import Farm, Animal, Weigthing


class FarmSerializer(serializers.ModelSerializer):

    class Meta:
        model = Farm
        fields = ('id', 'cnpj', 'name', 'user')


class AnimalSerializer(serializers.ModelSerializer):

    farm = serializers.PrimaryKeyRelatedField(queryset=Farm.objects.all())

    class Meta:
        model = Animal
        fields = ('id', 'type', 'ticket_number', 'farm')


class WeigthingSerializer(serializers.ModelSerializer):

    animal = serializers.PrimaryKeyRelatedField(queryset=Animal.objects.all())

    class Meta:
        model = Weigthing
        fields = ('id', 'animal', 'when', 'weigth')
        read_only_fields = ('when',)
