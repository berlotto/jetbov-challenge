from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from farms.permissions import FarmPermissions, AnimalPermissions, WeigthingPermissions
from rest_framework.authtoken.models import Token
from django.db.models.signals import post_save
from django.dispatch import receiver


class Farm(models.Model):
    name = models.CharField(max_length=200)
    cnpj = models.CharField(max_length=15, unique=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        permissions = FarmPermissions.as_django_permissions()


class Animal(models.Model):
    ANIMAL_TYPES = (
        ('bovino', 'Bovino'),
        ('equino', 'Equino'),
        ('caprino', 'Caprino'),
        ('ovino', 'Ovino'),
        ('suino', 'Suino'),
    )
    type = models.CharField(max_length=20, choices=ANIMAL_TYPES)
    ticket_number = models.CharField(max_length=50)
    farm = models.ForeignKey(Farm, on_delete=models.CASCADE)

    def __str__(self):
        return '{0} -> {1}'.format(self.farm.name, self.ticket_number)

    class Meta:
        permissions = AnimalPermissions.as_django_permissions()
        unique_together = (("ticket_number", "farm"),)


class Weigthing(models.Model):
    animal = models.ForeignKey(Animal, related_name='weights', on_delete=models.CASCADE)
    when = models.DateTimeField(auto_now=True)
    weigth = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return '{0} -> {1}, {2}'.format(self.animal.ticket_number, self.when.strftime('%Y-%m-%d'), self.weigth)

    class Meta:
        permissions = WeigthingPermissions.as_django_permissions()


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
