from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from farms.serializers import FarmSerializer, AnimalSerializer, WeigthingSerializer
from farms.models import Farm, Animal, Weigthing
from farms.filters import WeigthingFilter


class FarmViewSet(viewsets.ModelViewSet):
    queryset = Farm.objects.all()
    serializer_class = FarmSerializer

    @action(methods=['get'], detail=True)
    def pesagens(self, request, pk=None):
        farm = self.get_object()
        pesagens = Weigthing.objects.filter(animal__farm=farm)
        serializer = WeigthingSerializer(pesagens, many=True)

        return Response(serializer.data)


class AnimalViewSet(viewsets.ModelViewSet):
    queryset = Animal.objects.all()
    serializer_class = AnimalSerializer


class WeigthingViewSet(viewsets.ModelViewSet):
    queryset = Weigthing.objects.all()
    serializer_class = WeigthingSerializer
    filter_class = WeigthingFilter
