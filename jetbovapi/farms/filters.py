from django_filters import rest_framework as filters
from .models import Weigthing, Farm


class WeigthingFilter(filters.FilterSet):

    farm = filters.ModelChoiceFilter(name='animal__farm', queryset=Farm.objects.all())

    class Meta:
        model = Weigthing
        fields = ('animal', 'farm')
