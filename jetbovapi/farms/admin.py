from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from farms.models import Animal, Farm, Weigthing

admin.site.register(Farm)
admin.site.register(Animal)
admin.site.register(Weigthing)


class FarmInline(admin.StackedInline):
    model = Farm
    can_delete = False
    verbose_name_plural = 'farms'


class UserAdmin(BaseUserAdmin):
    inlines = (FarmInline, )


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
