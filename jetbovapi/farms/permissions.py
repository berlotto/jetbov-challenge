from rest_framework.permissions import DjangoModelPermissions

__all__ = ['FarmPermissions', 'AnimalPermissions', 'WeigthingPermissions']


class CustomDjangoModelPermissions(DjangoModelPermissions):
    perms_map = {
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': [],
        'HEAD': [],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }


class BasePermission(object):

    @classmethod
    def as_django_permissions(cls):
        assert getattr(cls, 'VALUES'), (
            'You need to declare \'VALUES\' class variable with a list of dicts{name,description}.'
        )

        return tuple(
            [(val['name'], val['description'])
             for val in cls.VALUES]
        )


class FarmPermissions(BasePermission):

    VALUES = [
        {
            'name': 'view_farm',
            'description': 'Can view farms'
        },
    ]


class AnimalPermissions(BasePermission):

    VALUES = [
        {
            'name': 'view_animal',
            'description': 'Can view animals'
        },
    ]


class WeigthingPermissions(BasePermission):

    VALUES = [
        {
            'name': 'view_weigthing',
            'description': 'Can view Weigthings'
        },
    ]
