from rest_framework.test import APITestCase
from .models import Farm, Animal, Weigthing
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token


class ApiTestCase(APITestCase):

    def setUp(self):
        self.usuario = User.objects.create(username="usuario1", is_staff=True, is_superuser=True)
        self.token = Token.objects.get(user=self.usuario)
        self.auth_token = self.token.key

        self.farm = Farm.objects.create(name="Fazenda 33", cnpj="010101010101", user=self.usuario)

        Animal.objects.bulk_create([
            Animal(type='bovino', ticket_number="12345", farm=self.farm),
            Animal(type='bovino', ticket_number="67890", farm=self.farm),
            Animal(type='bovino', ticket_number="12346", farm=self.farm),
            Animal(type='equino', ticket_number="23456", farm=self.farm),
            Animal(type='caprino', ticket_number="11145", farm=self.farm),
            Animal(type='ovino', ticket_number="65656", farm=self.farm),
            Animal(type='suino', ticket_number="10981", farm=self.farm),
        ])

        self.animal1 = Animal.objects.get(ticket_number='12345')
        self.animal2 = Animal.objects.get(ticket_number='67890')

        Weigthing.objects.bulk_create([
            Weigthing(animal=self.animal1, weigth=151.66),
            Weigthing(animal=self.animal1, weigth=152.00),
            Weigthing(animal=self.animal1, weigth=153.44),
            Weigthing(animal=self.animal2, weigth=161.66),
            Weigthing(animal=self.animal2, weigth=166.00),
            Weigthing(animal=self.animal2, weigth=168.03),
        ])

        self.client.force_authenticate(user=self.usuario, token=self.auth_token)

    def test_list_farms(self):

        self.assertEqual(Animal.objects.count(), 7)

    def test_api_list_farm_correct_data(self):

        response = self.client.get('/api/farm/', format='json')

        self.assertEqual(
            response.json(),
            [{'id': 1, 'cnpj': '010101010101', 'name': 'Fazenda 33', 'user': 1}]
        )
        self.assertEqual(response.status_code, 200)

    def test_api_list_farm_length(self):

        response = self.client.get('/api/farm/', format='json')

        self.assertEqual(len(response.json()), 1)

    def test_api_add_farm_user_duplicated(self):
        response = self.client.post('/api/farm/', {
            'name': 'Fazenda 2',
            'cnpj': '55667788',
            'user': self.usuario.id
        }, format='json')

        self.assertEqual(response.json(), {'user': ['This field must be unique.']})
        self.assertEqual(response.status_code, 400)

    def test_api_add_farm(self):
        usuario = User.objects.create(username="maria", is_staff=True, is_superuser=True)

        response = self.client.post('/api/farm/', {
            'name': 'Fazenda 2',
            'cnpj': '55667788',
            'user': usuario.id
        }, format='json')

        self.assertEqual(response.json(), {'cnpj': '55667788', 'id': 2, 'name': 'Fazenda 2', 'user': 2})
        self.assertEqual(response.status_code, 201)

    def test_api_get_farm(self):

        response = self.client.get('/api/farm/{0}/'.format(self.farm.id), format='json')

        self.assertEqual(response.json(), {
            'name': 'Fazenda 33',
            'cnpj': '010101010101',
            'id': self.farm.id,
            'user': self.usuario.id})
        self.assertEqual(response.status_code, 200)

    def test_api_update_farm_name(self):

        response = self.client.patch('/api/farm/{0}/'.format(self.farm.id), {
            'name': 'Nova fazenda'
        }, format='json')

        self.assertEqual(response.json(), {
            'name': 'Nova fazenda',
            'cnpj': '010101010101',
            'id': self.farm.id,
            'user': self.usuario.id})
        self.assertEqual(response.status_code, 200)

    def test_api_update_farm_cnpj(self):

        response = self.client.patch('/api/farm/{0}/'.format(self.farm.id), {
            'cnpj': '09090808'
        }, format='json')

        self.assertEqual(response.json(), {
            'name': 'Fazenda 33',
            'cnpj': '09090808',
            'id': self.farm.id,
            'user': self.usuario.id})
        self.assertEqual(response.status_code, 200)

    def test_api_update_farm_cnpj_null(self):

        response = self.client.patch('/api/farm/{0}/'.format(self.farm.id), {
            'cnpj': None
        }, format='json')

        self.assertEqual(response.json(), {'cnpj': ['This field may not be null.']})
        self.assertEqual(response.status_code, 400)

    def test_api_update_farm_cnpj_big(self):

        response = self.client.patch('/api/farm/{0}/'.format(self.farm.id), {
            'cnpj': '5' * 100
        }, format='json')

        self.assertEqual(response.json(), {'cnpj': ['Ensure this field has no more than 15 characters.']})
        self.assertEqual(response.status_code, 400)

    def test_api_update_farm_pesagens(self):

        response = self.client.get('/api/farm/{0}/pesagens/'.format(self.farm.id), format='json')

        self.assertEqual(len(response.json()), 6)
        self.assertEqual(response.status_code, 200)
