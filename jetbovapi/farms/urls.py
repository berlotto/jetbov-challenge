from farms.api import FarmViewSet, AnimalViewSet, WeigthingViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'farm', FarmViewSet)
router.register(r'animal', AnimalViewSet)
router.register(r'weigthing', WeigthingViewSet)
